package com.coderdojo.zen.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Javadoc
 */
@SpringBootApplication
@EnableConfigurationProperties(UriConfiguration.class)
@RestController
public class Application {

	/**
	 * Sole constructor. (For invocation by subclass
	 * constructors, typically implicit.)
	 */
	Application() { /* Default Constructor */ }

	/**
	 * Javadoc
	 *
	 * @param args Example
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Javadoc
	 *
	 * @param builder Example
	 * @param uriConfiguration Example
	 * @return Example
	 */
	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder, UriConfiguration uriConfiguration) {
		String httpUri = uriConfiguration.getHttpbin();
		return builder.routes()
				.route(p -> p
						.path("/get")
						.filters(f -> f.addRequestHeader("Hello", "World"))
						.uri(httpUri))
				.route(p -> p
						.host("*.circuitbreaker.com")
						.filters(f -> f
								.circuitBreaker(config -> config
										.setName("mycmd")
										.setFallbackUri("forward:/fallback")))
						.uri(httpUri))
				.build();
	}

	/**
	 * Javadoc
	 *
	 * @return Example
	 */
	@GetMapping("/fallback")
	public Mono<String> fallback() {
		return Mono.just("fallback");
	}
}

/**
 * Javadoc
 */
@ConfigurationProperties
class UriConfiguration {

	/**
	 * Javadoc
	 */
	private String httpbin = "http://httpbin.org:80";

	/**
	 * Sole constructor. (For invocation by subclass
	 * constructors, typically implicit.)
	 */
	UriConfiguration() { /* Default Constructor */ }

	/**
	 * Javadoc
	 *
	 * @return Example
	 */
	public String getHttpbin() {
		return httpbin;
	}

	/**
	 * Javadoc
	 *
	 * @param httpbin Example
	 */
	public void setHttpbin(String httpbin) {
		this.httpbin = httpbin;
	}
}
